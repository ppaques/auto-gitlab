# Awfull hack for development but quite efficient
#import sys
#sys.path.insert(0,"/home/pierre/workspace/python-gitlab")
import gitlab
import math
import datetime
import argparse
import requests
import json

def print_mr(mr, marker = "!"):
    return " - ["+marker+str(mr.iid)+"]("+mr.web_url+") " + mr.title + "\n"

def emoji_from_percent(percent):
    if percent < 20:
        return ":white_large_square: :white_large_square: :white_large_square: :white_large_square: :white_large_square:"
    elif percent < 40:
        return ":white_check_mark: :white_large_square: :white_large_square: :white_large_square: :white_large_square: "
    elif percent < 60:
        return ":white_check_mark: :white_check_mark: :white_check_mark: :white_large_square: :white_large_square:"
    elif percent < 80:
        return ":white_check_mark: :white_check_mark: :white_check_mark: :white_check_mark: :white_large_square:"
    elif percent < 100:
        return ":white_check_mark: :white_check_mark: :white_check_mark: :white_check_mark: :white_check_mark:"
    else:
        return ":+1:  :ok_hand:"


def emoji_from_label(labels):
    if 'New Feature' in labels:
        return ":rocket:"
    elif 'Improvements' in labels:
        return ":rocket:"
    elif "Documentation" in labels:
        return ":books:"
    elif 'Bug' in labels:
        return ":bug:"
    elif 'Internal' in labels:
        return ":house:"
    else:
        return ":moyai:"

def user_link(username, weburl):
    return "[@%s](%s)" % ( username, weburl )

def mr_link(number, web_url):
    return "[!%s](%s)" %(number, web_url)

def get_date_from_iso(date_str):
    if date_str is None:
        date_str = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%fZ')

    return datetime.datetime.strptime(date_str, '%Y-%m-%dT%H:%M:%S.%fZ')


def get_date_from_date(date_str, end_of_day=False):
    if end_of_day:
        return datetime.datetime.strptime(date_str+"T23:59:59.999Z", '%Y-%m-%dT%H:%M:%S.%fZ')
    else :
        return datetime.datetime.strptime(date_str, '%Y-%m-%d')



arg_parser = argparse.ArgumentParser( description = "Search pattern for iteration" )
#arg_parser.add_argument( "--title" , help="Search iterations with title matching this pattern", required=True)
arg_parser.add_argument( "--gitlab" , help="Gitlab Instance URL", default="https://gitlab.com")
arg_parser.add_argument( "--token" , help="Gitlab Token", required=True)
arg_parser.add_argument( "--group" , help="Group id on which searching for Iteration", required=True)
arg_parser.add_argument( "--pignore" , help="Coma separated list of project id to ignore while making MR list", default="", required=False)
arg_parser.add_argument( "--spec_close" , action='append', help="Add Specific close condition: Usage:`ProjectID,Tag1,Tag2,Tag3`. This will consider as closed condition for project id: the fact the issue is closed or the fact the tag is present.", required=False)

arguments = arg_parser.parse_args()

TAG="<!--AUTO-->"

proj_close_condition = { }
if arguments.spec_close is not None:
    for spec in arguments.spec_close:
        split = spec.split(",")
        try:
            if len(split) > 1:

                proj_close_condition[int(split[0])] = split[1:]
        except ValueError:
            continue

gl = gitlab.Gitlab(arguments.gitlab, private_token=arguments.token)
g = gl.groups.get(arguments.group)
iterations = g.iterations.list(state= "opened")

for it in iterations:
    print( "Evaluating: %s\n" % it.id)
    issues = g.issues.list(iteration_id=it.id , all=True)

    users_list = dict()
    users_list["unassigned"] = dict()
    users_list["unassigned"]['name'] = "No one"
    users_list["unassigned"]['username'] = "unassigned"
    users_list["unassigned"]['web_url'] = "#"

    for iss in issues:
        if iss.assignees is not None:
            for assignee in iss.assignees:
                if assignee['username'] not in users_list:
                    users_list[assignee['username']] = assignee

    # compensate the start date with week-end
    compensated_start_date = (datetime.datetime.strptime(it.start_date, '%Y-%m-%d') - datetime.timedelta(days=2)).strftime('%Y-%m-%d')

    # search issues assigned during th work
    # user_done_this sprint = xxx
    # Goal: measure interrupt rate
    if it.description is not None:
        description = it.description.partition(TAG)[0] + TAG
    else:
        description = TAG
    description +=  "\n## Planning\n"

    for user in users_list.values():
        user['planned_issues_count'] = 0
        user['planned_issues_done'] = 0
        user['planned_issues_percent'] = 0
        user['unplanned_rate_percent'] = 100
        user['unplanned_issues'] = []
        user['issues'] = []

        description += "- %s\n" % ( user_link (user['name'], user['web_url']) )

        for issue in issues:
            if len(issue.assignees) == 0:
                issue.assignees.append(users_list["unassigned"])

            if any(u.get('username', "unassigned") == user["username"] for u in issue.assignees):
                user['issues'].append(issue)
                ok = ":white_large_square:"
                user['planned_issues_count'] = user['planned_issues_count'] + 1

                # special condition for success management using labels
                label_closed_issue = False
                if issue.project_id in proj_close_condition:
                    label_closed_issue = any(x in issue.labels for x in proj_close_condition[issue.project_id])  

                done = False
                state_label = "~""State::Unknown"""
                if issue.state == "closed" or label_closed_issue:
                    ok = ":white_check_mark:"
                    state_label = "~""State::Done"""
                    done = True
                    user['planned_issues_done'] = user['planned_issues_done'] + 1
                else:
                    for label in issue.labels:
                        if label.startswith("State::"):
                            state_label = "~""%s""" % label
                            break

                description += "  - %s %s  [#%s](%s): %s %s \n" % ( ok,emoji_from_label(issue.labels), issue.iid, issue.web_url, state_label, issue.title )
                if user['planned_issues_count']  > 0 :
                    user['planned_issues_percent'] = math.floor(user['planned_issues_done']*100 / user['planned_issues_count'])

        # Get issues assigned but not in sprint
        worked_issues = g.issues.list(assignee_username = user["username"], scope="all", updated_after=compensated_start_date, updated_before=it.due_date, iteration_id="None", all=True)
        for wiss in worked_issues:
            if wiss in issues:
                continue
            if "State::TODO" in wiss.labels or \
                "State::Doing" in wiss.labels or \
                "State::Validation" in wiss.labels or \
                wiss.state == "closed":
                user['unplanned_issues'].append(wiss)
            if len(user['unplanned_issues']) > 0  and user['planned_issues_count'] > 0:
                user['unplanned_rate_percent'] = math.floor(len(user['unplanned_issues'])*100 / user['planned_issues_count'])

    description += "\n\n## Achievments\n"

    for user in users_list.values():
        if user['username'] == "unassigned":
            continue
        description += "- %s:\n" % user_link(user['name'], user['web_url'])
        description += "  - Score: %s\n" % emoji_from_percent(user['planned_issues_percent'])
        if len(user['unplanned_issues']) > 0 :
            list_issue = ""
            for wiss in user['unplanned_issues']:
                list_issue += " [#%s](%s)" % (str(wiss.iid) , wiss.web_url)
            description += "  - Unplanned issues: %s %s\n" % ( str(len(user['unplanned_issues'])), list_issue )


    description += "\n\n## Merge Requests\n"
    mrs = g.mergerequests.list(state='merged', order_by='updated_at', updated_after=compensated_start_date, updated_before=it.due_date, all=True)
    # sort MR by date and project
    # sort by merged date
    mrs = sorted(mrs, key=lambda k: get_date_from_iso(k.merged_at))

    merge_request_by_project = {}

    for mr in mrs:
        if mr.project_id in arguments.pignore.split(","):
            continue
        if get_date_from_date(compensated_start_date) < get_date_from_iso(mr.merged_at) < get_date_from_date(it.due_date, end_of_day=True):
            # show only merged merge requests during this iteration
            if mr.assignee is None:
                mr.assignee = users_list["unassigned"]
            # add MR
            if mr.project_id not in merge_request_by_project.keys():
                merge_request_by_project[mr.project_id] = {}

            if mr.target_branch not in merge_request_by_project[mr.project_id].keys():
                merge_request_by_project[mr.project_id][mr.target_branch] = []

            merge_request_by_project[mr.project_id][mr.target_branch].append(mr)
        else:
            #print("skipped mr due to date:" + mr.title)
            pass

    # now updating using the new sorted merge request list
    for project in merge_request_by_project.keys():
        proj = gl.projects.get(project)
        description += "- Project **%s**\n" % proj.name
        for target_branch in merge_request_by_project[project].keys():
            description += "  - **%s**\n" % target_branch

            for mr in merge_request_by_project[project][target_branch]:
                description += "    - %s %s: %s (%s)\n" % ( emoji_from_label(mr.labels), mr_link(mr.iid, mr.web_url),  mr.title,  user_link(mr.assignee['name'],mr.assignee['web_url']) )

    # print("\n")
    # print("\n")
    # print("\n")
    # print(description)
    # print("\n")
    # print("\n")
    # print("\n")
    # #iteration REST API not support PUSH.
    # #   - Let's use the graphql Mode
    query = r"""[{"operationName":"updateIteration","variables":{"input":{"groupPath":"%s","description":%s,"id":%s}},"query":"mutation updateIteration($input: UpdateIterationInput!) {\n  updateIteration(input: $input) {\n    iteration {\n      id\n      title\n     startDate\n      dueDate\n      __typename\n    }\n    errors\n    __typename\n  }\n}\n"}]""" % ( g.full_path , json.dumps(description), str(it.id))

    headers= {'Authorization': "Bearer {}".format(arguments.token), 'Content-Type': 'application/json'}
    r = requests.post(arguments.gitlab+"/api/graphql", query, headers= headers)
    if r.status_code == 200:
        print ("Updated OK (200)")
        print(r.content)
    else:
        print ("error?")
        print(r.request.headers)
        print(r.status_code)
        print(r.content)
        print(r.text)
        print(r.headers)
