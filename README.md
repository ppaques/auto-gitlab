# Gitlab automatic changelog and sprint

## Features :rocket:

- Automatically generate changelog inside milestones using `Merge requests`
- Allow custom text
- Allow to link with other milestones
- Automatic Tag filling inside related projects
- Automatic run using Gitlab-ci schedules

##  How it works?
- It uses milestones of gitlab
  - It can be *Issue based*.
  - It can be *Merge request based*
- It uses titles of issues or merge request
- It uses labels of your issues/merge request
- It uses assigne to link the contributor

Concretely, to have painless changelog you only need to keep your issues/merge request with correct title, milestone and labels.

## Concretely:
- Create a milestone
- Add inside a specific tag:
  - `<!--AUTO-->`  if you want a changelog based on merge request
  - `<!--AUTOI-->` if you want a changelog base on issues
- Link your issues/merge requests with the milestone
- Use labels to mark tags:
  - `Improvements`, `New Feature` are sorted with a rocket :rocket:
  - `Safety` is sorted a safety change :safety_vest: (note they are always shown and can be repeated across categories)
  - `Security` is sorted a with a lock :lock:
  - `Bugs` are sorted with a bug :bug:
  - `Internal` are sorted with a :home:
  - `unknown` no known labels use :moyai: because I'am Pierre Paques, and :moyai: comes from Paques's island

## Use expert features
- **Add a custom text**:
  - All what is above the tag `<!--AUTO-->` is left unchanged
- **Automatic filling of tags on projects** (in order to create release in gitlab)
  - Add `<!--TAG:name_of_you_tag-->`
  - The system will automatically seek for tags in relevant projects.
  - You can add as many tags as you want (if you tagged several names across several projects)
- **Merge different milestones together**
  - Add `<!--REL:name_of_the_other_milestone-->`
  - This will go on the related milestone and takes its merge requests or issues.
  - It is useful after you performed several milestones but you want to merge changelog together (use case: release candidates) 
- **Configure CI to work automatically**
  - Fork/mirror this project on your namespace
  - Activate shared runner for your project
  - Go to schedule page
  - configure the desired rate
  - configure the following environment variable
    - `CI_AUTO_TYPE` : `changelog`
    - `GITLAB_GROUP` : The group id you work with
    - `GITLAB_PROJECT`: the project id you want to work with
    - `GITLAB_PTAGS`: The projects where to look for tags
    - `GITLAB_TOKEN`: A gitlab token which has access to the project
- **Force an update**
  - Go on the project Gitlab
  - Launch manually the job changelog



## Why this project
I manage a small team of developper (10-15) which is split inside 2 separates scrum teams, our target is several small embedded devices where we cannot do `continuous delivery` due to the cost of the data.

Our process is mainly:
  - Commercials/application engineer gives us issues to resolve
  - We plan them in a sprint
  - We implement a solution for several branches (we basically have at least 3 branchs to maintain)
  - We gives changelog to application engineer
  - They use the artifact to manage the deployent.


This scripts allow us to reduce day to day annoying things with gitlab:
  - Tag an issue inside multiple milestones (as a bug is resolved inside multiples sw branhc)
    - we use the merge request
  - Prepare the sprint and make a retrospective:
    - Prepare the sprint: Automatically add all tickets of defined users to a defined iteration
    - Make the retrospective:
      - Check planned vs unplanned work using the iteration
      - List all merge request during the sprint
