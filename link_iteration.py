import gitlab
import argparse
import os


arg_parser = argparse.ArgumentParser( description = "Automatically update the issues that are assigned to the desired iteration" )
arg_parser.add_argument( "--iteration" , help="Iteration title", required=True)
arg_parser.add_argument( "--labels" , help="Labels to evaluate", default="TODO,Doing,Validation", required=False)
arg_parser.add_argument( "--pignore" , help="coma separated list of ignored project", default="14651575,12214529,10981146", required=False)

arguments = arg_parser.parse_args()

GITLAB_TOKEN=os.getenv("GITLAB_TOKEN")
GITLAB_GROUP_ID=os.getenv("GITLAB_GROUP_ID")

gl = gitlab.Gitlab('https://gitlab.com', private_token=GITLAB_TOKEN)
g = gl.groups.get(GITLAB_GROUP_ID)
iterations = g.iterations.list(search=arguments.iteration)
if len(iterations) == 0:
    print("Iteration %s not found" % arguments.iteration)
    exit(1)
if(len(iterations) >1):
    print("We found more than one iteration:\n")
    for it in iterations:
        print(" - %s\n" % it.title)
    exit(1)

iteration = iterations[0]
iteration_issues = iteration.issues(all=True)
iteration_issues_ids = []
for iss in iteration_issues:
    iteration_issues_ids.append(iss.id)

# get all issues that have tags and no iteration
issues = []
for label in arguments.labels.split(","):
    issues.extend( g.issues.list(all=True, labels=label, state="opened") )


for iss in issues:
    if str(iss.project_id) in arguments.pignore.split(","):
        print ("Skipping (project ignore) %s: %s" % (iss.references["relative"], iss.title))
        continue
    if iss.id in iteration_issues_ids:
        print ("Already linked %s: %s" % (iss.references["relative"], iss.title))
        continue

    # if weight is 0 set it to 1
    if iss.weight == 0:
        iss.weight = 1
        iss.save()

    proj = gl.projects.get(iss.project_id, lazy=True)
    editable_issue = proj.issues.get(iss.iid, lazy=True)

    # for now the only "api way" is  to use note commands:
    try:
        editable_issue.notes.create({'body': '/remove_iteration'})
    except:
        pass

    try:
        editable_issue.notes.create({'body': '/iteration %s' % iteration.title})
    except:
        print("ERROR linking %s: %s" % (iss.references["relative"], iss.title))

    print("Linking %s: %s" % (iss.references["relative"], iss.title))
