import gitlab
import argparse
import re
import pprint

AUTO_TAG="<!--AUTO-->"
AUTOI_TAG="<!--AUTOI-->"
LINK_MILESTONE=r'<!--LINK:(.*)-->'
RELEASE_TAG=r'<!--TAG:(.*)-->'

def print_mr(mr, marker = "!"):
    return " - ["+marker+str(mr.iid)+"]("+mr.web_url+") " + mr.title + "\n"

def get_milestone_changelog(milestone, is_issue = False):

    # changes
    changes = {}
    changes['improve'] = []
    changes['bugs'] = []
    changes['internal'] = []
    changes['unknown'] = []
    changes['safety'] = []
    changes['security'] = []
    changes['contributors'] = dict()

    # Find all mr/issues
    if is_issue:
        merge_requests = milestone.issues(state="closed",all=True)
    else:
        merge_requests = milestone.merge_requests(all =True)


    for mr in merge_requests:
        if not is_issue and mr.state != "merged":
            continue
        elif is_issue and mr.state != "closed":
            pass

        if 'Safety' in mr.labels:
            changes['safety'].append(mr)
        elif 'New Feature' in mr.labels:
            changes['improve'].append(mr)
        elif 'Improvements' in  mr.labels:
            changes['improve'].append(mr)
        elif 'Security' in mr.labels:
            changes['security'].append(mr)
        elif 'Bug' in mr.labels:
            changes['bugs'].append(mr)
        elif 'Internal' in mr.labels:
            changes['internal'].append(mr)
        else:
            changes['unknown'].append(mr)

        # limit commiters
        if not mr.assignee:
            continue
        elif not "name" in mr.assignee:
            continue
        elif mr.assignee["name"] in changes['contributors']:
            changes['contributors'][mr.assignee["name"]] = changes['contributors'][mr.assignee["name"]]+1
        else:
            changes['contributors'][mr.assignee["name"]] = 1

    return changes



def update_changelog_for_milestones(group, milestones, is_issue=False, potential_projects = [ ]):

    for milestone in milestones:
        if milestone.state == "closed":
            continue
        print("Starting: %s" % milestone.title)
        changes = get_milestone_changelog(milestone, is_issue= is_issue )


        # find linked milestones
        linked_milestones = re.findall(LINK_MILESTONE, milestone.description)
        for m in linked_milestones:
            m2 = group.milestones.list(title = m)
            if m2:
                changes2 = get_milestone_changelog(m2[0])
                changes['improve'] = list(set().union(changes['improve'], changes2['improve']))
                changes['bugs'] = list(set().union(changes['bugs'], changes2['bugs']))
                changes['internal'] = list(set().union(changes['internal'], changes2['internal']))
                changes['unknown'] = list(set().union(changes['unknown'], changes2['unknown']))
                for k,v in changes2['contributors'].items():
                    if k in changes['contributors']:
                        changes['contributors'][k] += v
                    else:
                        changes['contributors'][k] = v

        # change description
        new_desc = "\n"
        marker = "#" if is_issue else "!"


        if len(changes['safety']) > 0:
            new_desc = new_desc + "\n\n:safety_vest: **Safety Changes**:\n"
            for l in changes['safety']:
                new_desc = new_desc + print_mr(l,marker)
                potential_projects.append(str(l.project_id))


        if len(changes['improve']) > 0:
            new_desc = new_desc + "\n\n:rocket: **Enhancements**:\n"
            for l in changes['improve']:
                new_desc = new_desc + print_mr(l,marker)
                potential_projects.append(str(l.project_id))

        if len(changes['bugs']) > 0:
            new_desc = new_desc + "\n\n:bug: **Bugs**:\n"
            for l in changes['bugs']:
                new_desc = new_desc + print_mr(l,marker)
                potential_projects.append(str(l.project_id))

        if len(changes['security']) > 0:
            new_desc = new_desc + "\n\n:lock: **Security Changes**:\n"
            for l in changes['security']:
                new_desc = new_desc + print_mr(l,marker)
                potential_projects.append(str(l.project_id))



        if len(changes['unknown']) > 0:
            new_desc = new_desc + "\n\n:moyai: **Others:**\n"
            for l in changes['unknown']:
                potential_projects.append(str(l.project_id))
                new_desc = new_desc + print_mr(l,marker)

        if len(changes['internal']) > 0:
            new_desc = new_desc + "\n\n:house: **Internal:**\n"
            for l in changes['internal']:
                potential_projects.append(str(l.project_id))
                new_desc = new_desc + print_mr(l,marker)

        if len(changes['contributors']) > 0:
            new_desc = new_desc + "\n\n:baby: **Contributors:**\n"
            for k,v in changes['contributors'].items():
                new_desc = new_desc + "- "+ k + " ("+ str(v) +")\n"


        if is_issue:
            tag = AUTOI_TAG
        else:
            tag = AUTO_TAG

        milestone.description = milestone.description.partition(tag)[0] + tag + new_desc

        # filter potential projects
        # update tag lists
        list_releases = {}
        tag_new_desc = milestone.description +"\n\n[Milestone](%s)" % milestone.web_url
        potential_releases = re.findall(RELEASE_TAG, milestone.description)
        if len(potential_releases) > 0 :
            potential_projects = list(set(potential_projects))
            for release_name in potential_releases:
                for pid in potential_projects:
                    p = gl.projects.get(pid)
                    try:
                        rel = p.releases.get(release_name)
                        rel.description = tag_new_desc
                        rel.milestones = [ milestone.title ]
                        rel.save()
                        list_releases[p.name] = rel._links["self"]
                    except gitlab.exceptions.GitlabGetError as ex:
                        print("not found: Try Creating release in %s" % p.name)
                        try:
                            rel = p.releases.create({'name': release_name, 'tag_name': release_name, 'description':tag_new_desc, 'milestones': [ milestone.title ] })
                            list_releases[p.name] = rel._links["self"]
                        except gitlab.exceptions.GitlabCreateError as ex:
                            pass
                        except gitlab.exceptions.GitlabHttpError as ex:
                            pass

        release_list =  "\n\n:package: **Release List:**\n"
        for p,n in list_releases.items():
            release_list = release_list + "- ["+p + "]("+ n+")\n"

        milestone.description = milestone.description + release_list

        #print(milestone.description)
        # Todo Store linked releases :) 
        print("done")
        milestone.save()







arg_parser = argparse.ArgumentParser( description = "Automatic changelog based on merge requests or issues changelog." )
arg_parser.add_argument( "--gitlab" , help="Gitlab Instance URL", default="https://gitlab.com")
arg_parser.add_argument( "--token" , help="Gitlab Token", required=True)
arg_parser.add_argument( "--group" , help="Group id on which searching for milestones")
arg_parser.add_argument( "--project", help="Project id on which searching for milestones. This one has precedence over group")
arg_parser.add_argument( "--ptags" , help="Coma separated list of Project id that the system will add to seek tags to generates a release. (added even if there are no merge request in the project)", required=False)
arg_parser.add_argument( "--debug", help="turn on debug mode", action='store_true', default=False, dest='debug')

arguments = arg_parser.parse_args()

if arguments.ptags is not None:
    potential_project_tags = arguments.ptags.split(",")
else:
    potential_project_tags = []

# Get data from
gl = gitlab.Gitlab(arguments.gitlab, private_token=arguments.token)

if arguments.debug:
    gl.enable_debug()

if arguments.project is not None:
    g = gl.projects.get(arguments.project, lazy=True)
elif arguments.group is not None:
    g = gl.groups.get(arguments.group, lazy=True)
else:
    print("A project or group id is mandatory")
    exit(1)

auto = g.milestones.list(search=AUTO_TAG, state="active")
if len(auto) > 0 :
    update_changelog_for_milestones(g, auto, is_issue=False, potential_projects=potential_project_tags)

autoi = g.milestones.list(search=AUTOI_TAG, state="active")
if len(autoi) > 0 :
    update_changelog_for_milestones(g, autoi,  is_issue=True, potential_projects=potential_project_tags)

